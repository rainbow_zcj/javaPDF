package com.zcj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class ZcjLogbackServiceApplication {
	private static final	Logger logger = LoggerFactory.getLogger(ZcjLogbackServiceApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ZcjLogbackServiceApplication.class, args);
		logger.info("info..................");
		logger.debug("debug..................");
		logger.error("error..................");
		logger.warn("warn..................");
		logger.trace("trace..................");
	}
}
