package com.zcj.pdf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.pdf.BaseFont;

import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * pdf 工具类
 * 
 * @author zhengchengjiao
 *
 */
public class Html2Pdf {
	static Logger logger = Logger.getLogger(Html2Pdf.class);

	/***
	 * 生成PDF文档
	 * 
	 * @param dataMap
	 * @param tempFile
	 * @param inputFile
	 * @param outputFile
	 * @return
	 * @throws Exception
	 */
	public static boolean convertHtmlToPdf(ModelAndView dataMap, String outputFile,String baseURL,String templateName) {
		logger.info("HTML to PDF......");
		String htmlFile=baseURL+"pdf/"+System.currentTimeMillis()+".html";
		try {
			// ======想模板中填充数据
			createWordOrHtml(dataMap, templateName, htmlFile, baseURL+"templates/");

			// ======将HTML转成pdf
			OutputStream os = new FileOutputStream(outputFile);
			ITextRenderer renderer = new ITextRenderer();
			renderer.setDocument(htmlFile);
			// 解决中文支持问题
			renderer.getFontResolver().addFont(baseURL + "font/kaiti_GB2312.ttf", BaseFont.IDENTITY_H, true);
			
			renderer.getSharedContext().setBaseURL(baseURL);
			renderer.layout();
			renderer.createPDF(os);
			os.flush();
			os.close();
			return true;
		} catch (Exception e) {
			logger.info("HTML to PDF failed.......", e);
			return false;
		}finally {
			File file=new File(htmlFile);
			if(file.exists()){
//				file.delete();
			}
		}

	}

	/**
	 * 根据模板，动态填充数据，生成word或HTML文件
	 * 
	 * @param dataMap
	 * @param templateName
	 * @param filePath
	 * @param temPath
	 * @throws Exception
	 */
	public static void createWordOrHtml(ModelAndView dataMap, String templateName, String filePath, String temPath)
			throws Exception {
		try {
			// 创建配置实例
			Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);

			// 设置编码
			configuration.setDefaultEncoding("UTF-8");

			// ftl模板文件统一放至 com.lun.template 包下面
			// configuration.setClassForTemplateLoading(null, temPath);

			// 输出文件
			File temFile = new File(temPath);

			// 如果输出目标文件夹不存在，则创建
			if (!temFile.getParentFile().exists()) {
				temFile.getParentFile().mkdirs();
			}
			configuration.setDirectoryForTemplateLoading(temFile);

			// 获取模板
			Template template = configuration.getTemplate(templateName);

			// 输出文件
			File outFile = new File(filePath);

			// 如果输出目标文件夹不存在，则创建
			if (!outFile.getParentFile().exists()) {
				outFile.getParentFile().mkdirs();
			}

			// 将模板和数据模型合并生成文件
			Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "UTF-8"));

			// 生成文件
			template.process(dataMap.getModel(), out);

			// 关闭流
			out.flush();
			out.close();
		} catch (Exception e) {
			logger.info("freemarker data exception.......", e);
			throw e;
		}

	}

}
