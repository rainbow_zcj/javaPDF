package com.zcj.pdf;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.servlet.ModelAndView;

public class PdfTest {
	
	
	public static void main(String[] args) {
		
		
		//数据
		ModelAndView dataMap=new ModelAndView();
		dataMap.addObject("user", "zcj");
		dataMap.addObject("time", new Date());
		
		//注意图片路径必须是加“file:/”，否则生成的PDF文档里不能显示图片
		dataMap.addObject("imgUrl", "file:/F:/test/11.png");
		List<Object>userList=new ArrayList<>();
		dataMap.addObject("userList", userList);
		Map<String,Object> personList=new HashMap<>();
		dataMap.addObject("personList", personList);
		String baseURL=PdfTest.class.getResource("/").getPath();
		System.out.println(baseURL);
		String outputFile=baseURL+"pdf/"+System.currentTimeMillis()+".pdf";
		System.out.println(outputFile);
		Html2Pdf.convertHtmlToPdf(dataMap, outputFile, baseURL,"pdfDemo.html" );
		
		
	}

}
